import hashlib
import os
import sys

FOLLOWLINKS = False


#a script to find redundant files by comparing md5 hashes
#maybe revisit this some other time

def gen_hash(filename):

    cur_file = open(filename, 'r', encoding = "ISO-8859-1")
    file_content = cur_file.read().encode("utf-8")
    cur_file.close()

    file_hash = hashlib.md5()
    file_hash.update(file_content)

    return file_hash.hexdigest()

def get_list_files(root_dir):
    iterator = os.walk(root_dir, followlinks=FOLLOWLINKS)

    for directory in iterator:
        for files_dir in directory[2]:
            full_path = directory[0] + '/' + files_dir
            yield full_path

def find_redundant_files(root_dir):
    file_signatures = {}
    duplicate_file_entries = {}

    file_list = get_list_files(root_dir)

    for cur_file in file_list:
        file_hash = gen_hash(cur_file)

        if file_hash in file_signatures:
            if file_hash not in duplicate_file_entries:
                duplicate_file_entries[file_hash] = []
                duplicate_file_entries[file_hash].append(file_signatures[file_hash])
                duplicate_file_entries[file_hash].append(cur_file)
            else:
                duplicate_file_entries[file_hash].append(cur_file)
        else:
            file_signatures[file_hash] = cur_file

    duplicate_entries_list = []

    for _, entries in duplicate_file_entries.items():
        duplicate_entries_list.append(entries)

    return duplicate_entries_list

def main():
    if len(sys.argv) > 1:
        dupe_list = find_redundant_files(sys.argv[1])
    
        for dupe_entry in dupe_list:
            print(dupe_entry)
    else:
        print("ERROR: please provide directory in command line parameters")

if __name__ == "__main__":
    main()
